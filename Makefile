.PHONY: all tips docs

all: tips docs compile

tips:
	cat docs/_tips_tricks.rst docs/_tips_tricks/*.rst > docs/tips_tricks.rst

docs:
	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/basics.org -f org-rst-export-to-rst --kill
	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/simulations.org -f org-rst-export-to-rst --kill
	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/trajectories.org -f org-rst-export-to-rst --kill
	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/models.org -f org-rst-export-to-rst --kill
	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/docs.org -f org-babel-tangle --kill

compile:
	make -C docs/ html
