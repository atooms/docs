Atooms documentation
====================

[![pypi](https://img.shields.io/pypi/v/atooms.svg)](https://pypi.python.org/pypi/atooms/)
[![version](https://img.shields.io/pypi/pyversions/atooms.svg)](https://pypi.python.org/pypi/atooms/)
[![license](https://img.shields.io/pypi/l/atooms.svg)](https://en.wikipedia.org/wiki/GNU_General_Public_License)
[![pipeline](https://framagit.org/atooms/atooms/badges/master/pipeline.svg)](https://framagit.org/atooms/atooms/badges/master/pipeline.svg)

**atooms** is a high-level Python framework for simulations of interacting particles, such as molecular dynamics or Monte Carlo simulations.

This repository ships its [documentation](https://atooms.frama.io/docs/).

Authors
-------
Daniele Coslovich: https://www.units.it/daniele.coslovich/
