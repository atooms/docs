Atooms
======

**atooms** is a Python framework for simulations of interacting particles. It makes it easy to develop simulation and analysis tools using an expressive language, without sacrificing efficiency. To achieve this, atooms relies on backends written in C, CUDA or Fortran.

.. warning::

   The documentation is **under construction**.

---------

Documentation
-------------

.. toctree::
   :caption: Getting started

   overview
   installation

.. toctree::
   :caption: Core packages

   basics
   trajectories
   simulations

.. toctree::
   :caption: Backends

   native
   third_party
	     
.. toctree::
   :caption: Frontends
   :maxdepth: 2
	     
   database
   pantarei
   correlation/index
   landscape

.. toctree::
   :caption: Extras
   
   tips_tricks
