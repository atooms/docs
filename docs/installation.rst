Installation
============

The core package can be installed from the Python package index
::

   pip install atooms

Alternatively, from the code repository

::
   
   git clone https://framagit.org/atooms/atooms.git
   cd atooms
   make install
   
Additional frontends (component packages) can be installed as follows

::

   pip install atooms-<frontend>
   
where <frontend> is the name of the frontend.
