Overview
========

**atooms** is a high-level Python framework for simulations of
interacting particles, such as molecular dynamics or Monte Carlo simulations.

It is composed by a core package, which provides a consistent interface to the
basic objects of particle-based simulations, and feature packages built on top of it to implement complex simulation methods and analysis tools. 

Feature packages are available from the `atooms main repository <https://framagit.org/atooms>`__. They are installed in the
``atooms`` namespace to prevent name clashing. This allows frontends to evolve independent of the underlying core package.

Design principles
-----------------

- Focus on a simple and expressive interface
- API refined over the years towards consistency
- Modular and extensible design via namespace packages
- Semantic versioning - for what is worth
- Easy to interface: in-house codes and custom formats are first-class citizens
- Support for efficient simulation backends, with a focus on GPU codes

Quick start
-----------

Here is a small example for setting up a mixture of two types of
particles, A and B, in a periodic elongated cell. The number density is
set to unity.

.. code:: python

   from atooms.system import System

   system = System(N=64)
   system.replicate(times=4, axis=0)
   system.composition = {'A': 128, 'B': 128}
   system.density = 1.0

Particles in the central part of the cell get a random displacement and
are folded back into the simulation cell

.. code:: python

   import numpy

   for p in system.particle:
       if abs(p.position[0]) < system.cell.side[0] / 4:
           p.position += 0.5 * (numpy.random.random() - 0.5)
           p.fold(system.cell)
   system.show('ovito')

|image7|

Simulation data are stored in trajectory files, which are easy to
manipulate and convert with atooms. Here, we write the system species
and positions in a single-frame trajectory file using the `xyz
format <https://en.wikipedia.org/wiki/XYZ_format>`__.

.. code:: python

   from atooms.trajectory import TrajectoryXYZ

   with TrajectoryXYZ('input.xyz', 'w') as th:
       th.variables = ['species', 'position']  # actually, this is the default
       th.write(system)

The trajectory file can now be used to start a simulation using one the
available `simulation
backends <https://atooms.frama.io/atooms/tutorial/simulations.html>`__
or your own code.

.. |image7| image:: https://framagit.org/atooms/atooms/-/raw/master/snapshot.png
